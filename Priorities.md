*As of Feb 2021:*


These are my priorities:


* **Making Product Management data and customer driven. Specifically**
  * Data Driven through
    - Alignment on Product North Star Metric
    - Prioritization of data instrumentation in product for performance indicators
    - Including data into decision making process
  * Customer Driven through
    - Problem Validation Upfront
    - Lookbacks to learn and improve validation
    - Prioritizing with business context
    
   
* **Team Health. Specifically**
  * Empowerment - PMs are able to exercise their autonomy
  * Engagement - PMs are enaged and performing at the top of thier capabilities 
  * Accountability - PMs clearly understand what they are accountable

* **Product Depth. Specifically**
  * Learn product well to understand each stage, value, usage, market trends, direction
  * Help teams spot abilities to create cross-stage differentiation and priortize it
  * Include User Experience & Design as a partner during the ideation and prioritization phase 
