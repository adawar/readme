I took 4 months before writing the README. Why? I prefer to be open to listen to any and all perspectives and felt that having a README that shares my preferences meant that others have to change how they talk to me.

Having said that, I've found GitLab to be an amazing, open, transparent, and self-aware organization. Therefore I feel that by writing this README I'm not imposing my "preferences" on others, but instead sharing a "shortcut". It will help you connect to me so that we can collectively accomplish great things together and build trust faster.


* **Strategic Bent**: I like to take a long term strategic stance and focus on user value as well as buyer value. 
* **Principled Approach**: There are many ways to approach any given situation. In order to help make decisions, it is extremely helpful to have principles. Principles are guideposts that help everyone take many small decisions every day without having to necessarily consult leadership. They help keep us all aligned. This is especially true for product direction and decisions.
* **Getting to the heart of the problem**: I like the [Five Whys](https://www.mindtools.com/pages/article/newTMC_5W.htm) approach. I think it helps get better appreciation of the problem space and leads to better solutions.
* **Explicitly state your assumptions**: Often we make decisions because we have some assumptions about the situation. I like to spell those assumptions explicitly. Why? Because if one of those assumptions turns out to be wrong, then we know we need to revisit and change our direction. It also helps get everyone of the same page together. I've often found that many disagreements are not really about the actual proposal, but our underlying assumptions about the situation. 
* **Seperating Facts from Opinions:** Facts are hard truths. Opinions are our judgement based on that truth. I prefer to seperate them so that we know what is what. This again helps us understand and figure out a path forward easily. As one famous CEO said (I don't have an attribution)-- "If we are going by opinions, lets go with mine. But if you have data, then lets talk"
* **Positive Demeanor:** I've been told I tend to be cheery even when discussing serious topics. It helps me focus on the situation more effectively. I feel decisions should be driven from a point of realistic optimism and not pessimistic fear. 
* **Impatience** Sometimes I feel that I know the answer and we should just go ahead and listen to me :) This is something I'm working on. If you find me in this situation, know that this is an area of improvement. Call it out to me. I will do better.
* **Inclusion:** My first lessons on inclusion came from my younger sister. She challenged my notions of gender equality and showed me how biased the world is. I care about inclusion. But I'm as much a product of patriarchy as most others. I've seen myself use terms like "Hey guys!" and continue to try to be more inclusive. If you see me using non-inclusive language or behavior. Call it out. I will thank you for helping me improve.



